
public class Node {

	int data;
	Node left;
	Node right;
	
	public Node(){
		data = 0;
		left = null;
		right = null;
	}
	
	public Node(int data){
		this.data = data;
		left = null;
		right = null;
	}
}
