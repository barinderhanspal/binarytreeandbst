import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;


public class BTreeUtil {

	static protected Node root = null;

	public static boolean searchRec(Node root, int key){
		if(root == null)
			return false;
		else if(root.data == key)
			return true;
		else if(root.data > key) 
			return searchRec(root.left, key);
		else 
			return searchRec(root.right, key);
	}

	public static boolean searchIter(Node root, int key){
		if(root != null) {
			Node current = root;
			while(current != null){
				if(current.data == key)
					return true;
				else if (current.data > key)
					current = current.left;
				else 
					current = current.right;
			}
		}
		return false;
	}

	public static Node insertRec(Node root, Node node){
		if(root == null)
			return node;
		if(root.data > node.data){
			root.left = insertRec(root.left, node);
		} else {
			root.right = insertRec(root.right, node);
		}
		return root;
	}

	public static void printInorder(Node root){
		if(root == null)
			return;
		else {
			printInorder(root.left);
			System.out.println(root.data);
			printInorder(root.right);
		}
	}

	public static Node remove(Node root, int data){
		if(root == null)
			return null;

		if(root.data > data)
			root.left = remove(root.left, data);
		else if (root.data < data)
			root.right = remove(root.right, data);
		else {
			// found the node to remove

			// case 1: leaf node
			if(root.left == null && root.right == null)
				root = null;
			// case 2: left child is null
			else if(root.left == null)
				root = root.right;
			else if(root.right == null)
				root = root.left;

			else {
				// case 3: has left and right nodes
				Node successor = root.right;
				while(successor.left != null)
					successor = successor.left;
				root.data = successor.data;
				root.right = remove(root.right, successor.data);
			}
		}
		return root;
	}

	public static Node findMinimum(Node root){
		if(root == null || root.left == null)
			return root;
		return findMinimum(root.left);
	}

	public static boolean isLesserThanValue(Node root, int value){
		if(root == null)
			return true;

		if(root.data > value)
			return false;

		return isLesserThanValue(root.left, value) 
				&& isLesserThanValue(root.right, value);
	}

	public static boolean isGreaterThanValue(Node root, int value){
		if(root == null)
			return true;
		if(root.data < value)
			return false;

		return isGreaterThanValue(root.left, value)
				&& isGreaterThanValue(root.right, value);
	}

	public static boolean isBST(Node root){
		if(root == null)
			return true;

		return isLesserThanValue(root.left, root.data) 
				&& isGreaterThanValue(root.right, root.data)
				&& isBST(root.left) && isBST(root.right);
	}

	public static boolean isBSTEff(Node root){
		return isBSTEffHelper(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}

	public static boolean isBSTEffHelper(Node root, int lowerBound, int upperBound){
		if(root == null)
			return true;

		return root.data < upperBound && root.data > lowerBound
				&& isBSTEffHelper(root.left, lowerBound, root.data)
				&& isBSTEffHelper(root.right, root.data, upperBound);
	}

	public static void printLevelOrder(Node root){
		if(root == null)
			return;
		Queue<Node> queue = new LinkedList<Node>();
		queue.add(root);
		while(!queue.isEmpty()){
			Node current = queue.poll();
			System.out.println(current.data);
			if(current.left != null)
				queue.add(current.left);
			if(current.right != null)
				queue.add(current.right);
		}
	}

	public static void printDFS(Node root){
		if(root == null)
			return ;
		Stack<Node> stack = new Stack<Node>();
		stack.push(root);
		while(!stack.empty()){
			Node current = stack.pop();
			System.out.println(current.data);
			if(current.left != null)
				stack.push(current.left);
			if(current.right != null)
				stack.push(current.right);
		}
	}

	public static int getNumLeaves(Node root){
		if(root == null)
			return 0;

		if(root.left == null && root.right == null)
			return 1;

		return getNumLeaves(root.left) + getNumLeaves(root.right);
	}
	public static int getHeight(Node root){
		if(root == null)
			return -1;
		return Math.max(getHeight(root.left), getHeight(root.right)) + 1;
	}

	public static int getDiameter(Node root){
		if(root == null)
			return 0;

		return Math.max(getHeight(root.left) + getHeight(root.right) + 1,
				Math.max(getDiameter(root.left), getDiameter(root.right)));
	}

	public static boolean isHeightBalanced(Node root){
		if (root == null)
			return true;

		return (isHeightBalanced(root.left) && isHeightBalanced(root.right) 
				&& (Math.abs(getHeight(root.left) - getHeight(root.right)) <= 1));
	}

	public static boolean hasPathSum(Node root, int value){
		if(root == null)
			return value == 0;

		return hasPathSum(root.left, value - root.data)
				|| hasPathSum(root.right, value - root.data);
	}

	/*public static Node insertIter(Node root, Node node){
		if(root == null){
			return node;
		} else {
			Node current = root;
			while(current != null){
				if(current.data > node.data){
					current = current.left;
					if(current)
				} else {
					current = current.right;
				}

			}
		}
	}*/

	public static boolean printAncestors(Node root, Node node){
		if(root == null)
			return false;
		if(root.data == node.data)
			return true;

		if(printAncestors(root.left, node) || printAncestors(root.right, node)){
			System.out.println(root.data);
			return true;
		} else {
			return false;
		}
	}

	public static boolean isSumTree(Node root){
		if(root == null)
			return true;
		return (root.data * 2 == isSumTreeHelper(root));
	}

	public static int isSumTreeHelper(Node root){
		if(root == null)
			return 0;
		return root.data + isSumTreeHelper(root.left) + isSumTreeHelper(root.right);
	}

	public static void printNodesAtKLevel(Node root, int level){
		if (root == null)
			return;
		if(level == 0)
			System.out.println(root.data);

		printNodesAtKLevel(root.left, level -1);
		printNodesAtKLevel(root.right, level -1);
	}

	public static void printLevelOfNode(Node root, Node node){
		printLevelOfNodeHelper(root, node, 0);
	}
	public static void printLevelOfNodeHelper(Node root, Node node, int level){
		if(root == null)
			return;
		if(root.data == node.data)
			System.out.println(level);
		else {
			printLevelOfNodeHelper(root.left, node, level + 1);
			printLevelOfNodeHelper(root.right, node, level + 1);
		}

	}

	public static void printVerticalSums(Node root){
		java.util.HashMap<Integer, Integer> vIndexMap = new java.util.HashMap<Integer, Integer>();
		calculateVerticalSum(root, 0, vIndexMap);
		System.out.println(vIndexMap.entrySet());
	}
	public static void calculateVerticalSum(Node root, int verticalIndex, 
			java.util.HashMap<Integer, Integer > vIndexMap){
		if(root == null)
			return;

		calculateVerticalSum(root.left, verticalIndex - 1, vIndexMap);

		if(vIndexMap.containsKey(verticalIndex)){
			int currentSum = vIndexMap.get(verticalIndex);
			vIndexMap.put(verticalIndex, currentSum + root.data);
		} else {
			vIndexMap.put(verticalIndex, root.data);
		}
		calculateVerticalSum(root.right, verticalIndex + 1, vIndexMap);
	}

	public static boolean isComplete(Node root){
		if(root == null)
			return true;

		if(root.left == null && root.right != null)
			return false;

		if(root.left != null && root.right != null){
			if((root.left.left == null || root.left.right == null)
					&& (root.right.left != null || root.right.right != null))
				return false;
		}

		return isComplete(root.left) && isComplete(root.right);
	}

	public static void printBoundary(Node root){
		if(root == null)
			return;
		printLeftBoundary(root);
		System.out.println("leafs");
		printLeafNodes(root);
		System.out.println("right");
		printRightBoundary(root.right);
	}

	public static void printLeafNodes(Node root){
		if(root == null)
			return;
		if(root.left == null && root.right == null)
			System.out.println(root.data);

		printLeafNodes(root.left);
		printLeafNodes(root.right);
	}

	public static void printLeftBoundary(Node root){
		if(root == null)
			return;

		if(root.left != null) {
			System.out.println(root.data);
			printLeftBoundary(root.left);
		} else if (root.right != null){
			System.out.println(root.data);
			printLeftBoundary(root.right);
		}
	}

	public static void printRightBoundary(Node root){
		if(root == null)
			return;

		if(root.right != null){
			printRightBoundary(root.right);
			System.out.println(root.data);
		} else if (root.left != null){
			printRightBoundary(root.left);
			System.out.println(root.data);
		}
	}

	public static Node findLCA(Node root, Node node1, Node node2){
		if(root == null)
			return root;
		if(node1.data < root.data && node2.data < root.data){
			return findLCA(root.left, node1, node2);
		} else if (node1.data > root.data && node2.data > root.data){
			return findLCA(root.right, node1, node2);
		} else {
			return root;
		}
	}
	
	
	
	/*public static int findHeightFromNode()

	public static int findDistanceBetweenNodes(Node root, Node node1, Node node2){
		Node lca = findLCA(root, node1, node2);
		
		return 
	}*/
	
	public static void printRootToLeaves(Node root){
		java.util.Stack<Node> stack = new java.util.Stack<Node> ();
		
		printRootToLeavesHelper(root, stack);
	}

	
	
	private static void printRootToLeavesHelper(Node root, Stack<Node> stack) {
		if(root == null){
			return;
		}
		
		stack.push(root);
		if(root.left == null && root.right == null){
			System.out.println(stack);
			stack.pop();
		}
		//incomlepte
		
	}
	
	public static void printLineByLineBFS(final Node root){
		
		if(root == null)
			return;
		Node marker = new Node();
		Queue<Node> q = new LinkedList<Node>();
		q.add(root);
		q.add(marker);
		
		while(!q.isEmpty()){
			Node current = q.remove();
			
			if(current == marker){
				System.out.println();
				if(!q.isEmpty())
					q.add(marker);
			} else {
			
			System.out.print(current.data + " ");
			if(current.left != null)
				q.add(current.left);
			if(current.right != null)
				q.add(current.right);
			}
		}
		
	}
	
	
	public static void printLineLevel(Node root){
		
		if(root == null)
			return;
		Node marker = new Node();
		Node current = root;
		Queue<Node> q = new LinkedList<Node>();
		
		
		q.add(current);
		q.add(marker);
		
		while(!q.isEmpty()){
			current = q.poll();
			
			if(current == marker){
				System.out.println();
				if(!q.isEmpty()){
					q.add(marker);
				}
			} else {
				System.out.print(current.data + " ");
				if(current.left != null)
					q.add(current.left);
				if(current.right != null)
					q.add(current.right);
			}
		}
	}
	
	public static int findMaxRootToLeaf(Node root){
		if(root == null)
			return 0;
		
		return root.data + 
				Math.max(findMaxRootToLeaf(root.left), findMaxRootToLeaf(root.right));
	}

	public static void printMaxPath(Node root){
		int maxSum = findMaxRootToLeaf(root);
		printPathToMaxSum(root, maxSum);
	}
	
	public static boolean printPathToMaxSum(Node root, int maxSum){
		if(root == null)
			return (maxSum == 0);
		
		if(printPathToMaxSum(root.left, maxSum - root.data) 
				|| printPathToMaxSum(root.right, maxSum - root.data)){
			System.out.println(root.data);
			return true;
		} else {
			return false;
		}
	}

	public static int findKSmallest(Node root, int k){
	
		ArrayList<Integer> list = new ArrayList<Integer>();
		findKSmallestHelper(root, k, list);
		if(list.size() >= k){
			return list.get(k -1);
		}
		return -1;
	}
	
	private static void findKSmallestHelper(Node root, int k,
			ArrayList<Integer> list) {
	
			if(root == null || list.size() == k){
				return;
			} 
			
			findKSmallestHelper(root.left, k, list);
			list.add(root.data);
			findKSmallestHelper(root.right, k, list);
		
	}
	
	public static void printNodesBetweenLevels(Node root, int lowLevel, int highLevel){
		if(root == null || lowLevel > highLevel)
			return;
		
		Queue<Node> queue = new LinkedList<Node>();
		Node current = root, marker = new Node();
		
		int level = 1;
		queue.add(current);
		queue.add(marker);
		
		while(!queue.isEmpty()){
			current = queue.poll();	
			if(current == marker){
				level++;
				if(!queue.isEmpty())
					queue.add(marker);
			} else {
				if(level >= lowLevel && level <= highLevel)
					System.out.println(current.data);
				if(current.left != null)
					queue.add(current.left);
				if(current.right != null)
					queue.add(current.right);
			}
		}
	}
	
	public static void printNodesBetweenLevels2(Node root, int lLevel, int hLevel){
		printNodesBetweenLevelsHelper(root, 1, lLevel, hLevel);
	}
	
	/*boolean areMirrorTrees(BinaryTree root1, BinaryTree root2) {
	    if (root1 == null && root2 == null) {
	        return true;
	    }
	    if (root1 == null || root1 == null) {
	        return false;
	    }
	    return root1->data == root2->data && 
	               areMirrorTrees(root1->left, root2->right)  && 
	               areMirrorTrees(root1->right, root2->left);
	}*/

	
	private static void printNodesBetweenLevelsHelper(Node root, int level,
			int lLevel, int hLevel) {
		if(root == null)
			return;
		if(level >= lLevel && level <= hLevel)
			System.out.println(root.data);
		
		printNodesBetweenLevelsHelper(root.left, level + 1, lLevel, hLevel);
		printNodesBetweenLevelsHelper(root.right, level + 1, lLevel, hLevel);
		
	}

	public static void convertToLeftSumTree(Node root){
		
		if(root == null)
			return;
		
		convertToLeftSumTree(root.left);
		root.data = root.data + (root.left == null ? 0 : root.left.data);
		convertToLeftSumTree(root.right);
	}
	
	public static void findDeepestNodeBFS(Node root){
		if(root == null)
			return;
		
		Node current = root;
		Node marker = new Node(1);
		Node prev = null;
		Queue<Node> q = new LinkedList<Node>();
		q.add(current);
		q.add(marker);
		
		while(!q.isEmpty()){
			
			current = q.poll();
			
			if(current == marker){
				if(!q.isEmpty())
					q.offer(marker);
				else 
					System.out.println(prev.data);
				
			} else {
				
				
				
				if(current.left != null)
					q.add(current.left);
				if(current.right != null)
					q.add(current.right);
				
				prev = current;
			}
		}
		
	}
	
	public static void findDeepestNode(Node root){
		int height = getHeight(root);
		printNodesAtKLevel(root, height);
	}
	

	public static void writeTreeToFile(Node root, StringBuilder sb){
		if(root == null) {
			sb.append(" # ");
			return;
		}
		
		sb.append(" " + root.data + " ");
		writeTreeToFile(root.left, sb);
		writeTreeToFile(root.right, sb);
	}
	
	public static Node createTreeFromFile(Node root, Scanner input){
		if(!input.hasNext())
			return null;
		String ch = input.next();
		if(ch.equals("#")){
			return null;
		}
		
		Node current = new Node(Integer.parseInt(ch));
		current.left = createTreeFromFile(root.left, input);
		current.right = createTreeFromFile(root.right, input);
		
		return current;
	}
/*	
	public static Node findRightMostCousin(Node root, int rootData){
		if(root == null)
			return null;
		
		Queue<Node> q = new LinkedList<Node>();
		Node prev = null;
		Node marker = new Node(-11);
		
		q.add(root);
		q.add(marker);
		boolean foundNode = false;
		
		while(!q.isEmpty()){
			
			Node current = q.poll();
			
			if(current == marker){
				if(foundNode == true){
					if(q.peek() != prev)
						return prev;
					else {
						return null;
					}
				}
				if(!q.isEmpty()){
					q.add(marker);
				}
			} else {[]
				
				if(current.data == rootData)
					foundNode = true;
				if(current.left != null)
					q.add(current.left);
				if(current.right != null)
					q.add(current.right);
			}
			prev = current;
		}
		return null;
	}*/
	
	public static Node findNextRightCousin(Node root, int nodeData){
		
		if(root == null)
			return root;
		
		Node current = root;
		int level = 1;
		
		Queue<Node> nq = new LinkedList<Node>();
		Queue<Integer> lq = new LinkedList<Integer>();
		nq.add(current);
		lq.add(level);
		
		while(!nq.isEmpty()){
			current = nq.poll();
			level = lq.poll();
			
			if(current.data == nodeData){
				if(nq.isEmpty() || lq.peek() != level){
					return null;
				} else {
					return nq.poll();
				}
			}
			
			if(current.left != null){
				nq.offer(current.left);
				lq.offer(level + 1);
			}
			if(current.right != null){
				nq.offer(current.right);
				lq.offer(level + 1);
			}
		}
		
		return null;
	}
	
	/**
	 * DOES NOT HANDLE EDGE CASES
	 * 
	 * @param root
	 * @param data
	 * @return
	 */
	public static Node findRightMostNodeForKey(Node root, int data){
		if(root == null)
			return root;
		
		Queue<Node> q = new LinkedList<Node>();
		Node prev = null;
		Node marker = new Node(-111);
		boolean foundElement = false;
		
		q.offer(root);
		q.add(marker);
		
		while(!q.isEmpty()){
			Node current = q.poll();
			
			if(current == marker){
				
				if(foundElement){
					return prev;
				}
				if(!q.isEmpty()){
					q.add(marker);
				}
				
			} else {
				
				if(foundElement){
					prev = current;
				}
				
				if(current.data == data){
					foundElement = true;
				}
				if(current.left != null){
					q.add(current.left);
				}
				if(current.right != null){
					q.add(current.right);
				}
				
				
			}
		}
		
		return null;
	}
	

	public static Node getDeepestNode(Node root){
		if(root == null)
			return null;
		
		Node current = root;
		Queue<Node> q = new LinkedList<Node>();
		q.add(current);
		
		while(!q.isEmpty()){
			current = q.poll();
			
			if(current.left != null)
				q.add(current.left);
			if(current.right != null)
				q.add(current.right);
		}
		
		return current;
	}
	
	public static void main(String args[]){

	
		
		root = new Node(10);
		root.left = new Node(4);
		root.right = new Node(12);
		root.left.left = new Node(3);
		root.left.right = new Node(5);
		root.right.left = new Node(11);
		//root.left.left.left = new Node(1);
		
		System.out.println(getDeepestNode(root).data);
		//System.out.println(findCommonAncestor(root, 3, 5));

	//	System.out.println(findRightMostNodeForKey2(root, 11).data);
		//System.out.println(findNextRightCousin(root, 5).data);
		//findDeepestNode(root);
		//findDeepestNodeBFS(root);
		/*printInorder(root);
		convertToLeftSumTree(root);
		System.out.println(" --- ");
		printInorder(root);*/
		/*printNodesBetweenLevels(root, 1, 2);
		System.out.println("&&&&&&&&&&&");
		printNodesBetweenLevels2(root, 1, 2);
		*//*	 root = insertRec(root, new Node(50));
		 root = insertRec(root, new Node(30));
		 root = insertRec(root, new Node(20));
		 root = insertRec(root, new Node(40));
		 root = insertRec(root, new Node(70));
		 root = insertRec(root, new Node(60));
		 root = insertRec(root, new Node(80));*/

		//System.out.println(findKSmallest(root, 11));
		/*System.out.println(findMaxRootToLeaf(root));
		
		printMaxPath(root);
		*///printInorder(root);
		// print
		// printLevelOrder(root);
		//System.out.println(hasPathSum(root, 14));
		//printVerticalSums(root);
		//System.out.println(isComplete(root));
	//	printBoundary(root);
		//printLineLevel(root);
		//printLineByLineBFS(root);
	}
}
